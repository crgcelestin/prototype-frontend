import getUsersData2 from "@/routers/UsersData";

export default async function Home() {

  const usersData = await getUsersData2()

  return (
    <div>
      <h1
        className="head-text text-left"
      > NXTGEN LANDING PAGE </h1>
    </div>
  );
}
