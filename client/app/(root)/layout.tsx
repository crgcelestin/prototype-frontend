// import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "../globals.css";
import TopBar from "@/components/shared/globalLayout/topbar";
import LeftSideBar from "@/components/shared/globalLayout/leftbar";
import RightSideBar from "@/components/shared/globalLayout/rightbar";
import Home from "./page";
import BottomBar from "@/components/shared/globalLayout/bottombar";


const inter = Inter({ subsets: ["latin"] });

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <>
      <TopBar />
      <div className="mt-10">
        <main className="flex">
          <LeftSideBar />
          <section className="main-container">
            <div className="w-full max-w-4xl">
              <Home />
            </div>
          </section>
          <RightSideBar />
        </main>
      </div>
    </>
  );
}
