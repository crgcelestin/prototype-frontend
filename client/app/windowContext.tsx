// turns all code to only client side however as entire fe becomes client wrapped

'use client'

import React, {
    FC,
    createContext,
    useCallback,
    useEffect,
    useState
} from 'react'

export type WindowContextProps = {
    clientHeight: number,
    clientWidth: number;
};

// implement React Context that subscribes to window resizing events that provides current clientHeight, clientWidth in context
export const WindowContext = createContext<WindowContextProps>({
    clientHeight: 0,
    clientWidth: 0
});

export const WindowContextProvider: FC = ({ children }) => {

    // extract current height
    const getVh = useCallback(() => {
        return Math.max(
            document.documentElement.clientHeight || 0,
            window.innerHeight || 0
        )
    }, []);

    // extract current width
    const getVw = useCallback(() => {
        return Math.max(
            document.documentElement.clientHeight || 0,
            window.innerWidth || 0
        )
    }, []);

    // store state of context
    const [clientHeight, setVh] = useState<number>(getVh());
    const [clientWidth, setVw] = useState<number>(getVw());
    useEffect(() => {
        const handleResize = () => {
            setVh(getVh());
            setVw(getVw());
        }
        window.addEventListener('resize', handleResize);
        return () => {
            window.removeEventListener('resize', handleResize);
        };
    }, [getVh, getVw]);

    return (
        <WindowContext.Provider
            value={{ clientHeight, clientWidth }}
        >
            {children}
        </WindowContext.Provider>
    )
}

/*
    // adding to app context

    <WindowContextProvider>
    </WindowContextProvider>
*/
