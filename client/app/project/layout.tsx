const SingleProjectLayout = ({ children }) => {
    return (
        <div>
            <h1> single project page </h1>
            <div> {children} </div>
        </div>
    )
}

export default SingleProjectLayout
