const Post = async ({
    params,
}: {
    params: { slug: string }
}) => {
    return (
        <>
            <div> This is {`${params.slug}`}'s Private Profile</div>
        </>
    )
}

export default Post;
