// nav
'use client'
import { Inter } from 'next/font/google'
import Link from 'next/link'
import "./globals.css"
import { useParams } from 'next/navigation'

const inter = Inter({ subsets: ['latin'] })
const params = useParams<{ domain: string; profile: string }>
const links = [
    { href: '/', label: 'Home' },
    { href: '/feed', label: 'Feed' }, // main feed route
    { href: `/profile/private/profile`, label: 'Private Profile Page' }, // redirect to indv profile page user sees
    { href: `/profile/public/profile`, label: 'Public Profile Page' }, // redirect to indv profile page
    { href: '/post', label: 'Post' }, // redirect to indv post
    { href: '/project', label: 'Project' } // non published project
]

export default function RootLayout({
    children,
}: {
    children: React.ReactNode
}) {
    return (
        // has to become a nav that spans horizontally
        <html lang="en">
            <body className={inter.className}>
                <header>
                    <nav className='ml-2'>
                        <ul className='flex items-center p-2'></ul>
                        {links.map(link => (
                            <li key={link.href.toString()}>
                                <Link href={link.href}>{link.label}</Link>
                            </li>
                        ))}
                    </nav>
                </header>
                <div className='p-4'>{children}</div>
            </body>
        </html>
    )
}
