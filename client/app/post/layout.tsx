const SinglePostLayout = ({ children }) => {
    return (
        <div>
            <h1> single post page </h1>
            <div> {children} </div>
        </div>
    )
}

export default SinglePostLayout
