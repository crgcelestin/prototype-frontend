import PostView from "@/components/shared/posts/singlePost/SinglePost";

const Post = async () => {
    await new Promise((res) => res(0))
    const post1 = {
        id: 1,
        author: 'Mark Goodwill',
        author_role: 'Chemical Engineer',
        description: 'Arcu risus quis varius quam quisque id. Ut diam quam nulla porttitor massa id neque aliquam vestibulum. Volutpat lacus laoreet non curabitur gravida arcu ac tortor. Id interdum velit laoreet id donec ultrices tincidunt arcu non. ',
        image: '',
        project: null,
        time_posted: new Date(2018, 0O5, 0O5, 17, 23, 42, 11),
        likes: ['user3', 'user2'],
        comments: [
            {
                id: 1,
                commenter: 'user2',
                content: 'Nunc aliquet bibendum enim facilisis. Enim facilisis gravida neque convallis a cras semper auctor neque. Diam maecenas sed enim ut sem.',
                likes: [
                    'user1, user2'
                ]
            },
            {
                id: 2,
                commenter: 'user3',
                content: 'Varius duis at consectetur lorem. Vitae nunc sed velit dignissim sodales. Arcu non odio euismod lacinia at quis risus. Netus et malesuada fames ac. Accumsan in nisl nisi scelerisque eu ultrices vitae auctor eu. Ante in nibh mauris cursus.',
                likes: [
                    'user1'
                ]
            }
        ]
    }
    return post1
}

const SinglePost = async () => {
    const PostData = await Post()
    return (
        <PostView
            post={PostData}
        />
    )
}

export default SinglePost;
