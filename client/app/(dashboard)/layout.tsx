import CreatePost from "@/components/shared/posts/createPost/createPost"
import { useState } from "react"

const FeedLayout = ({ children }) => {

    const [searchQuery, setSearch] = useState({
        query: ""
    })

    const search = (query: string) => {
        // perform searching function in order to match to posts, etc...

    }

    function handleChange(e) {
        setSearch(e.target.value)
    }

    const handleSearch = (e) => {
        e.preventDefault()
        // const result = await fetch(`url/${searchQuery}`)
        setSearch({
            query: ""
        })
    }

    return (
        <main
        >
            <input
                type="text"
                placeholder='Search...'
                value='query'
                onChange={
                    (e) => {
                        setSearch({
                            query: e.target.value
                        })
                    }
                }
                onKeyUp={(e) => {
                    if (e.key === 'Enter') {
                        search(searchQuery.query)
                    }
                }}
            />
            <div>
                <form
                    onSubmit={handleSearch}
                >
                    <input
                        type='text'
                        value={searchQuery.query}
                        onChange={handleChange}
                    />
                </form>
            </div>
            <h1
                className="mb-5"
            > Start of Feed </h1>
            <span
            > <CreatePost /></span>
            <div> {children} </div>
        </main>
    )
}

export default FeedLayout
