'use client'

import Posts from "@/components/shared/posts/MultiplePosts"
import useGetPosts from "@/routers/PostData"
import { LoadingPage } from "@/components/shared/loading/LoadingPage"
import States from "@/routers/States/LoadingStates"

const Feed = () => {
    const postsData = useGetPosts()
    // need to add idle page
    // stand in loading comp
    switch (postsData[0]) {
        case States.IDLE:
            return <div> idle... </div>
        case States.LOADING:
            return <LoadingPage />
        case States.SUCCESS:
            const [, data] = postsData
            console.log(data)
            return (
                <div>
                    <Posts posts={data.posts} />
                </div>
            )
        case States.FAILURE:
            const [, error] = postsData
            return (
                <div>
                    {'Encountered an error retrieving post data'}
                    <span>
                        <ul>
                            <li
                                className="font-bold"
                            >
                                {error.status}
                            </li>
                            <li>
                                {error.statusText}
                            </li>
                            <span
                                className="font-thin"
                            >
                                <li>
                                    {error.data.name}
                                </li>
                                <li>
                                    {error.data.message}
                                </li>
                            </span>
                        </ul>
                    </span>
                </div>
            )
    }
}

export default Feed;

/*

    function Posts() {
        const result = useGetPosts()

        switch (result[0]) {
            case States.IDLE:
            return <div>idle</div>

            case States.LOADING:
            return <div>loading...</div>

            case States.SUCCESS: {
            // We can destructure here because our `context` is refined to the
            // correct shape by the switch statement. We can also skip
            // destructuring `state` and rename `context` to `data` in one step
            const [, data] = result

            return (
                <div>
                {data.posts.map(post => (
                    <div key={post.title}>
                    {post.title} {post.body}
                    </div>
                ))}
                </div>
            )
            }

            case States.FAILURE: {
            const [, error] = result
            return <div>{error.message}</div>
            }
        }
    }

*/
