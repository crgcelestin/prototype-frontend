// each post inherits post layout and takes in individual post data w/ interactivity
// interactivity is to include being able to like, share, ..etc
'use client'
import PostLayout from '../PostLayout'
import Link from 'next/link'
import Image from 'next/image'
import { useState, useMemo } from 'react'
import styles from './styles.module.css'
import dayjs from "dayjs";
import relativeTime from 'dayjs/plugin/relativeTime';
dayjs.extend(relativeTime)
import { Post, Project, Comment } from '@/routers/Types/ProjectTypes'
import { PassThrough } from 'stream'
import { randomUUID } from 'crypto'

// client component
const PostView = ({ post }: { post: Post }) => {

    // going to be used for making updates to be for likes, comments, etc..

    let uuid = randomUUID()

    const [Post, updatePost] = useState<Post>({
        id: 0,
        author: "",
        author_role: "",
        description: "",
        images: [""],
        projects: [],
        time_posted: new Date,
        likes: [""],
        comments: [],
        reposts: [""],
        shares: [""]
    })

    // good for dummy db, need to have this function to use post id in order to look at db
    const handleLike = (e: React.MouseEvent<HTMLButtonElement>) => {
        const action = e.target as HTMLButtonElement
        const value = action.value
        const updatedLikes = [...post.likes, value]
        updatePost({
            ...post, likes: updatedLikes
        })
        // perform method that subs out post in dummyposts for new post with different likes
    }

    const handleComment = () => {
        PassThrough
    }

    // function for rendering post images or project images
    const renderPost = ({
        post_content
    }: {
        post_content: Post
    }) => {
        // req conditional in jsx for post structure if multiple post images or multiple project images

        if (post_content.images) {
            // 1st case is if there is post.images, map over images, must handle situation where there are multiple

            const post_images_length = post_content.images.length

            if (post_content.images.length > 0) {
                return (
                    <div>
                        {post_content.images.map((image, index) => (
                            <Image
                                key={index}
                                src={image}
                                alt={`Post image ${index + 1}`}
                                width={200 / post_images_length}
                                height={200 / post_images_length}
                            />
                        ))}
                    </div>
                )
            }
        }
        if (!post_content.images && post_content.projects) {
            // 2nd case, no post.images, there is post.project.images, handle if multiple of project.images

            const project_length = post_content.projects.length

            // only want projects with images - find gets first instance of project that has images and [images] with elements in it
            // should probably convert to filter? and useMemo?

            // at some point for optimization need this to be an endpoint
            const filteredProjects = useMemo(() => post_content.projects?.find(
                (project) => project.images && project.images.length > 0
            ), [post_content.projects])

            const filterProjectsForImages = (projects: Project[], num_of_items: number) => {

            }

            // current implementation only maps over first project instead of all projects user can attach to post
            if (project_length > 0) {
                if (filteredProjects !== null) {
                    return (
                        <div>
                            {filteredProjects?.images ? (
                                <div>
                                    {filteredProjects.images?.map((image, index) => (
                                        <Image
                                            key={index}
                                            src={image}
                                            alt={`Post image ${index + 1}`}
                                            width={200 / project_length}
                                            height={200 / project_length}
                                        />
                                    ))}
                                </div>
                            ) : (
                                // testing purposes
                                <div>
                                    {'NONE'}
                                </div>
                            )}
                        </div>
                    )
                }
            }
        }

        // 3rd case, post.images, post.project.images are both null
        return (
            <div>
                <Image
                    src='/images/download.png'
                    alt='default image'
                    width={200}
                    height={200}
                />
            </div>
        )
    }

    // function for returning project subsection on Post
    const renderProjects = ({
        project_content
    }: {
        project_content: Project[] | null
    }) => {
        //handle if project content exists -> own subsection of post
        return (
            <div>

            </div>
        )
    }
    return (
        <PostLayout>
            <Link href={''}>
                <span className='font-bold '>
                    {post.author}
                </span>
            </Link>
            <div
                className={`font-thin ${styles.AuthorTimePosted}`}>
                <span>
                    {post.author_role}
                    <Link href={''}>
                        <span
                            className={`${styles.timePosted}`}
                        >
                            {`${dayjs(post.time_posted ? post.time_posted : '').fromNow()}`}
                        </span>
                    </Link>
                </span>
            </div>
            <Link href={''}>
                {
                    renderPost({ post_content: post })
                }
            </Link>
            <span>
                {post.description}
            </span>
            <div
                className={`flex-col items-center mt-2`}
            >
                <div
                    style={{ fontWeight: 'bold' }}
                >
                    {`${post.likes?.length} likes`}
                    <button
                        // may need to change to different implementation later oor have value be the user.id?
                        value={`user ${uuid}`}
                        onClick={handleLike}
                    >
                        Like
                    </button>
                </div>
                <ul
                    className={`ml-5`}
                >
                    {post.likes?.map((like) => (
                        <li
                            className={`${styles.ulstart}`}
                            key={like.toString()}>
                            {like}
                        </li>
                    ))}
                </ul>
            </div>
            <div
                className='mt-5 mb-5'
            >
                <span
                    style={{ fontWeight: 'bold' }}
                >
                    {`${post.comments?.length} comments`}
                </span>
                <ul>

                    {post.comments?.map((comment) => (

                        <li key={comment.id}>
                            <ul
                                className='mt-5 mb-5'
                            >
                                <li
                                >
                                    {comment.commenter}
                                </li>
                                <li
                                    className={`${styles.contentWrap}`}
                                >
                                    {comment.content}
                                </li>
                                <li
                                    className={`${styles.commentLikes}`}
                                >
                                    {` likes: ${comment.likes}`}
                                </li>
                            </ul>
                        </li>

                    ))}
                </ul>
            </div>
            <span
                style={{ fontWeight: 'bold' }}
            >
                {'reposts:'}
            </span>
            <div
                className='mb-5'
            >
                {post.reposts}
            </div>
            <span
                style={{ fontWeight: 'bold' }}
            >
                {'shares:'}
            </span>
            <div

            >
                {post.shares}
            </div>
            <div>
                {
                    renderProjects({ project_content: post.projects })
                }
            </div>
        </PostLayout >
    )
}


export default PostView
