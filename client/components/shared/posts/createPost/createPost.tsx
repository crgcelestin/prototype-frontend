'use client'
import React, { useState } from "react"
import styles from './styles.module.css'
import States from "@/routers/States/LoadingStates"
import { Post } from "@/routers/Types/ProjectTypes"

// client component
// add tr
const CreatePost = () => {

    const [postToCreate, setPost] = useState<Post>({
        id: 0,
        author: "",
        author_role: "",
        description: "",
        images: [""],
        projects: [],
        time_posted: new Date,
        likes: [""],
        comments: [],
        reposts: [""],
        shares: [""]
    })

    function submitPost(e) {
        // perform form submission, createPost api route
        //return alert (toast?) depending on idle, successful, unsucesseful - use states
        // dummyposts - add created post with inputs to be added to dummy data

    }
    const handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
        if (e.key === 'Enter') {
            console.log(postToCreate)
            e.preventDefault()
            submitPost(
                postToCreate
            )
        } else {
            //toast alert? or continue
        }
    }

    return (
        <div
            className="form-group"
        >
            <form
                onSubmit={submitPost}
                className={`${styles.inputGroup}`}
            >
                <label
                    htmlFor="identifier"
                >
                    <input
                        id='identifier'
                        placeholder="&nbsp;"
                        className={`${styles.formControl} form-input ${styles.iconPlaceholder}`}
                        value="description"
                        type="text"
                        onKeyDown={(e) => { handleKeyDown(e) }}
                        onChange={(e) => {
                            setPost({ ...postToCreate, description: e.target.value })
                        }}
                    />
                    <span
                        style={{ color: 'grey' }}

                    >
                        Write a post...

                    </span>
                    {/*
                        Have modals for each button interaction to allow for user selection when creating posts
                    */}
                    <button>
                        Photo
                    </button>
                    <button>
                        Video
                    </button>
                    <button>
                        Document
                    </button>
                    <button>
                        Project
                    </button>
                    <button
                        className={`${styles.submitButton}`}
                        onClick={(e) => { submitPost(e) }}
                    >
                        Post
                    </button>
                </label>
            </form>

        </div>
    )
}

export default CreatePost
