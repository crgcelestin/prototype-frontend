// contain std format for an individual post
import styles from './styles.module.css'

const PostLayout = ({ children }) => {
    return (
        <div>
            <h1> </h1>
            <div
                className={`${styles.constrainedDiv} ${styles.postContainer}`}
            > {children} </div>
        </div>
    )
}

export default PostLayout
