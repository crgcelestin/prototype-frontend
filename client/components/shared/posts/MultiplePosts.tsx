import { randomUUID } from "crypto"
import PostView from "@/components/shared/posts/singlePost/SinglePost"

const Posts = ({ posts }) => {
    return (
        <div>
            {posts.map((post) => (
                <div
                    style={{ width: '80vh' }}
                    className='flex gap-3 border-b p-4'
                    key={post.id}>
                    {/* Use keys.map to render each property of the post */}
                    <PostView
                        post={post}
                    />
                </div>
            ))}
        </div>
    )
}

export default Posts
