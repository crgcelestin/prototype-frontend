'use client'
import { useState, useEffect } from "react"
import States from "./States/LoadingStates"

type User = {
    id: Number,
    username: string,
    email: string,
    first_name: string,
    last_name: string,
    created_at: Date,
    is_active: Boolean,
    status: string,
    description: string,
    profile_photo: string,
    last_logged_in: Date
}
type UserLikes = {
    id: Number,
    post: string
}

type UserData = { posts: Array<User> }

const DummyUsers: UserData = [
    {},
    {}
]

const getUsersData = async () => {
    const users = await fetch('http://localhost:3002/users')
    if (users.ok) {
        try {
            const usersData = await users.json()
            return usersData
        } catch (e) {
            console.error(e)
        }
    } else {
        return { error: 'no user data found' }
    }
}

export default getUsersData;
