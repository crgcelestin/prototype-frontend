'use client'
import { useState, useEffect } from "react"
import States from "./States/LoadingStates"
import { Project, Comment, Post } from './Types/ProjectTypes'
// temp db for posts

// interactions - modals allowing for interactions
// likes, reposts, bookmarks can just be counters that are incremented
// shares -> separate modal that has share options

type ProjectData = { projects: Project[] }
type PostsData = { posts: Post[] }

// fix types
const DummyProjects: ProjectData = {
    projects: [
        {
            id: 1,
            owner: 'user2',
            type: 'Data Science',
            description: 'Vulputate dignissim suspendisse in est ante in nibh mauris cursus. Vitae tortor condimentum lacinia quis vel. Vitae congue mauris rhoncus aenean vel elit scelerisque. In ante metus dictum at tempor commodo ullamcorper a lacus.',
            requests: [
                'user1'
            ],
            is_private: true,
            user_access: [
                'user2', 'user3'
            ],
            images: null
        },
        {
            id: 2,
            owner: 'user3',
            description: 'Senectus et netus et malesuada fames. Malesuada fames ac turpis egestas integer. Odio eu feugiat pretium nibh. Ut ornare lectus sit amet. Neque gravida in fermentum et sollicitudin ac orci phasellus egestas. ',
            type: 'Chemical Engineering',
            is_private: false,
            requests: null,
            user_access: [
                'user3', 'user2'
            ],
            images: [
                'https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.che.psu.edu%2Findustry%2Fcapstone-design-mentor.aspx&psig=AOvVaw0IWKb6b34ztfWtMLK6h8ZF&ust=1715323614515000&source=images&cd=vfe&opi=89978449&ved=0CBIQjRxqFwoTCODlk8j8_4UDFQAAAAAdAAAAABAE',
                'https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.cbe.iastate.edu%2Fcbe-senior-design-projects%2F&psig=AOvVaw0IWKb6b34ztfWtMLK6h8ZF&ust=1715323614515000&source=images&cd=vfe&opi=89978449&ved=0CBIQjRxqFwoTCODlk8j8_4UDFQAAAAAdAAAAABAJ'
            ]
        },
        {
            id: 3,
            owner: 'user1',
            description: 'Amet commodo nulla facilisi nullam. Vel turpis nunc eget lorem dolor sed viverra ipsum nunc. Facilisi etiam dignissim diam quis enim lobortis scelerisque. Purus ut faucibus pulvinar elementum. Lacus suspendisse faucibus interdum posuere lorem ipsum dolor sit.',
            type: 'Mechanical Engineering',
            is_private: true,
            requests: [
                'user2'
            ],
            user_access: [
                'user1', 'user3'
            ],
            images: [
                'https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.researchgate.net%2Ffigure%2FAn-example-of-a-gearbox-as-a-mechanical-engineering-project_fig1_343032028&psig=AOvVaw2CFG3mhshN5rtjbFGFonLI&ust=1715324385307000&source=images&cd=vfe&opi=89978449&ved=0CBIQjRxqFwoTCLC9kbj__4UDFQAAAAAdAAAAABAEE'
            ]
        }
    ]
}
const DummyPosts: PostsData = {
    posts: [
        {
            id: 1,
            author: 'Mark Goodwill',
            author_role: 'Chemical Engineer',
            description: 'Arcu risus quis varius quam quisque id. Ut diam quam nulla porttitor massa id neque aliquam vestibulum. Volutpat lacus laoreet non curabitur gravida arcu ac tortor. Id interdum velit laoreet id donec ultrices tincidunt arcu non. ',
            images: null,
            projects: [DummyProjects.projects[2]],
            time_posted: new Date(2018, 0O5, 0O5, 17, 23, 42, 11),
            likes: ['user3', 'user2'],
            comments: [
                {
                    id: 1,
                    commenter: 'user2',
                    content: 'Nunc aliquet bibendum enim facilisis. Enim facilisis gravida neque convallis a cras semper auctor neque. Diam maecenas sed enim ut sem.',
                    likes: [
                        'user1, user2'
                    ]
                },
                {
                    id: 2,
                    commenter: 'user3',
                    content: 'Varius duis at consectetur lorem. Vitae nunc sed velit dignissim sodales. Arcu non odio euismod lacinia at quis risus. Netus et malesuada fames ac. Accumsan in nisl nisi scelerisque eu ultrices vitae auctor eu. Ante in nibh mauris cursus.',
                    likes: [
                        'user1'
                    ]
                }
            ],
            reposts: ['user2'],
            shares: ['user3']
        },
        {
            id: 2,
            author: 'Jane Williams',
            author_role: 'Data Science Student',
            description: ' Elit scelerisque mauris pellentesque pulvinar pellentesque habitant. Vel turpis nunc eget lorem dolor sed viverra ipsum nunc. Tempor commodo ullamcorper a lacus vestibulum sed arcu non.',
            images: ['https://plus.unsplash.com/premium_photo-1673688152102-b24caa6e8725?q=80&w=1032&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D'],
            time_posted: new Date("2024-05-11"),
            likes: ['user1', 'user3'],
            projects: [DummyProjects.projects[0]],
            comments: [
                {
                    id: 1,
                    commenter: 'user1',
                    content: 'Elit sed vulputate mi sit amet mauris. Et molestie ac feugiat sed lectus vestibulum mattis ullamcorper. Eu turpis egestas pretium aenean pharetra magna.',
                    likes: [
                        'user2, user3'
                    ]
                }
            ],
            reposts: ['user1'],
            shares: ['user3']
        },
        {
            id: 3,
            author: 'Mark Goodwill',
            author_role: 'Chemical Engineer',
            description: 'Arcu risus quis varius quam quisque id. Ut diam quam nulla porttitor massa id neque aliquam vestibulum. Volutpat lacus laoreet non curabitur gravida arcu ac tortor. Id interdum velit laoreet id donec ultrices tincidunt arcu non. ',
            images: [
                'https://images.squarespace-cdn.com/content/v1/61e9374e0434354049a258f9/690ac844-111a-4f3a-aa19-ff94b0ed4a53/Robot+Car.jpg,',
                'https://www.digipen.edu/sites/default/files/public/img/engineering/02-image/digipen-computer-engineering-2nd-year-four-legged-friend-im.jpg'
            ],
            time_posted: new Date(2018, 0O5, 0O5, 17, 23, 42, 11),
            likes: ['user3', 'user2'],
            projects: [DummyProjects.projects[1]],
            comments: [
                {
                    id: 1,
                    commenter: 'user2',
                    content: 'Nunc aliquet bibendum enim facilisis. Enim facilisis gravida neque convallis a cras semper auctor neque. Diam maecenas sed enim ut sem.',
                    likes: [
                        'user1, user2'
                    ]
                }
            ],
            reposts: ['user2'],
            shares: ['user3']
        },
        {
            id: 1,
            author: 'Mark Goodwill',
            author_role: 'Chemical Engineer',
            description: 'Arcu risus quis varius quam quisque id. Ut diam quam nulla porttitor massa id neque aliquam vestibulum. Volutpat lacus laoreet non curabitur gravida arcu ac tortor. Id interdum velit laoreet id donec ultrices tincidunt arcu non. ',
            images: null,
            time_posted: new Date(2018, 0O5, 0O5, 17, 23, 42, 11),
            likes: ['user3', 'user2'],
            projects: null,
            comments: [
                {
                    id: 1,
                    commenter: 'user2',
                    content: 'Nunc aliquet bibendum enim facilisis. Enim facilisis gravida neque convallis a cras semper auctor neque. Diam maecenas sed enim ut sem.',
                    likes: [
                        'user1, user2'
                    ]
                }
            ],
            reposts: ['user2'],
            shares: ['user3']
        }
    ]
}


type errorInfo = {
    name: string,
    message: string
}
type errorResponse = {
    status: number,
    statusText: string,
    data: errorInfo
}

type Results =
    | [States.IDLE, undefined]
    | [States.LOADING, undefined]
    | [States.SUCCESS, PostsData]
    | [States.FAILURE, errorResponse]


function useGetPosts(
): Results {
    const [result, setResult] = useState<Results>([States.IDLE, undefined])

    useEffect(() => {
        setResult([States.LOADING, undefined])
        setTimeout(() => {
            if (Object.keys(DummyPosts).includes('posts')) {
                setResult([
                    States.SUCCESS, DummyPosts
                ])
            } else {
                const postsError: errorResponse = {
                    status: 404,
                    statusText: 'Not Found',
                    data: {
                        name: 'Posts not Found',
                        message: 'Error Posts Not Found'
                    }
                }
                setResult(
                    [States.FAILURE, postsError]
                )
            }
        });
        // fetch('/api/posts')
        //     .then(res => res.json())
        //     .then(DummyPosts => {
        //         setResult([States.SUCCESS, DummyPosts])
        //     })
        //     .catch(error => {
        //         setResult([States.FAILURE, error])
        //     })
    }, [])

    return result
}

// updatePost
function updatePost(
    PostData: Post
): Post {
    // take in post via id with new data
    // find corresponding post with said id
    // update post
}
// createPost
// deletePost

export default useGetPosts;
