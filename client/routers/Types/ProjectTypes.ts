/*
enum ProjectTypes {

}
*/

export type Project = {
    id: number,
    owner: string,
    description: string,
    requests: Array<string> | null,
    type: string,
    is_private: boolean,
    user_access: Array<string>,
    images: Array<string> | null
}

export type Comment = {
    id: number,
    commenter: string,
    content: string,
    likes: Array<string>,
}

export type Post = {
    id: number,
    author: string,
    author_role: string,
    description: string,
    images: null | Array<string>,
    projects: null | Array<Project>,
    time_posted: Date,
    likes: Array<string>,
    comments: Array<Comment>,
    reposts: Array<string>,
    shares: Array<string>
}
