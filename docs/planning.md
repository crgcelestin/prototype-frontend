# Frontend Setup, Wireframes

## Setup, Framework Choice
### Confirmed
- Going with: [nextjs, tailwindcss, three.js]
    * working on this tutorial: [guide](https://www.youtube.com/watch?v=O5cmLDVTgAs)
    * skipped auth section

### MISC
- referred to Oauth2.0 site for OAuth Libs for NodeJs and it said PassportJS
    * Can use PassportJS as an OAuth2 approved library[auth](https://www.passportjs.org/packages/passport-oauth2/)
    * or use alt open source
        - Rationale
            ```
            SuperTokens is an open-core alternative to proprietary login providers like Auth0 or AWS Cognito. We are different because we offer: Open source: SuperTokens can be used for free, forever, with no limits on the number of users.
            ```

- preventing caching for components desired to be non-static
    * [NoStore](https://nextjs.org/docs/app/api-reference/functions/unstable_noStore#version-history)

## Wireframes
- [MIRO](https://miro.com/app/board/uXjVNS579PY=/#)
- [mockups](https://www.figma.com/file/fKO6WnYEZu5WWt09mM0R3c/Mockups?type=design&node-id=0-1&mode=design&t=nQFuOk5qS9qkSHto-0)
- [be diagram](https://www.figma.com/file/BsmUdn2PnN9JgkapukIpsG/NXTGEN-BE-DIAGRAM?type=design&node-id=0-1&mode=design)

### 03.03.24
- Installed three.js

### 04.15.24
- Was running into error `JSX element implicitly has type 'any' because no interface 'JSX.IntrinsicElements' exists`
    * performed `npm install --save-dev @types/react` to install React types

### 04.20.24
* in order to change local branch and match remote
    - git checkout main -> git branch -m <old> <new> -> git push origin HEAD:<old> -> git branch -u origin/<new> -> git push origin HEAD

### 04.26.24
- [x] T3 Tutorial
    * going to use trpc (ssg helper and prefetching - t3stack) + nextjs + mongoose (mdb) + zod (t3 stack) + ts
        - based off findings from tutorial
            * Resources:
                - [nextjs, trpc, mdb](https://medium.com/@harutyunabgaryann/nextjs-trpc-mongoose-49a330ccb7a2)
                - [ts, zod, mdb](https://zzdjk6.medium.com/typescript-zod-and-mongodb-a-guide-to-orm-free-data-access-layers-f83f39aabdf3)
- [ ] FE Masters NextJS
    * [x] Intro, [x] Prod Grade
- Aiming to integrate T3 stack tutorial with that of FE Masters in order to gain advantages from both approaches as well as with ts articles read
- Current Progress
    * currently have menu navigating to pages

## 04.28.24 -> ...
- Can invoke `npm run dev` for nxt-gen backend and then poll as its running at localhost server of be to plug in for fe for calls
- [x] installed trpc (routing)
- had to nuke globals.css file as it kept giving issues, going to redo custom styling and understand how to best set up globals

## 05.05.24
- Testing
    - Implement CI tool to build and test - unit testing, end to end testing
        - unit -> options: jest, mocha, Jasmine, karma
        - end to end -> options: cypress, playwright

## 05.07.24
- !Problem - we have nodejs be with a ts frontend using next js
    * going to have to pass jwts, credentials/handle security using be with fe being for cookies and their expiration i.e trigger end/start of sessions
*  For auth:
    1. use jwts/oauth to design auth enable routes/api to be utilized for fe in terms of sessions/account management

    - if we move to monrepo
        2. NextAuth.js which allows for custom ui, is prebuilt for next, and has a mongodb adapter
        - [Adapter from Auth.js docs](https://authjs.dev/getting-started/adapters/mongodb)
        - [NextAuth](https://nextjs.org/learn/dashboard-app/adding-authentication)

## 05.11
- This is a great first step to the actual markup
    * ![current Progress](./images/currentUI.png)
        * problems with it is the button and year moves as the screen moves instead of staying constant or the post box resizing as per the moving of the window
            * createPost
                * when you go to enter text, the input starts in the middle of the box and not the top
                * no action triggered for enter, need to use serverActions or useState and useContext for testing just on fe repo
            * indv posts
                - need to get their appearance simliar to mockup
    * need to implement rest of ui, side and top navs/ui comps
    * [x]fix postdata typing, nesting is messed for .map() as a result

## 05.17
- [using .find()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/find)

# Resources
* [Params](https://nextjs.org/docs/app/api-reference/file-conventions/page)
* [useParams](https://nextjs.org/docs/app/api-reference/functions/use-params)
* [Fetching, Caching, Revalidating](https://nextjs.org/docs/app/building-your-application/data-fetching/fetching-caching-and-revalidating)
* [Cookies](https://nextjs.org/docs/app/api-reference/functions/cookies)
* [headers](https://nextjs.org/docs/app/api-reference/functions/headers)
* [caching](https://nextjs.org/docs/app/building-your-application/caching)
* [theme ui](https://theme-ui.com)


# Server Actions v API Routes
- Recommended to use api routes for fetching server data
    * [source](https://www.youtube.com/watch?v=O8AmIELoxrc)
        * ![Api Routes](./images/apiRoutes.png)
        * ![Server Actions](./images/serverAction.png)
* [SWR Hook](https://swr.vercel.app/docs/getting-started)
* [fetching via sa](https://nextjs.org/docs/app/building-your-application/data-fetching/server-actions-and-mutations)

### Using Suspense
```js

// Can defer client component, library that takes a long time to load and is not critical to UI can be deferred from initial client bundle with React lazy (code splitting)

async function DelayedComponent()
  await new Promise((resolve) => setTimeout(resolve, 3000));
  return <div>Delayed Component</div>;
}

export default async function Page() {
  return (
    <div>
      <div>This is visible immediately</div>
      <Suspense fallback={<div>Loading Delayed Component...</div>}>
        <DelayedComponent />
      </Suspense>
    </div>
  );
}
```
### Using Lazy
```js
// Can defer client component, library that takes a long time to load and is not critical to UI can be deferred from initial client bundle with React lazy (code splitting)

'use client'
import { lazy, useState } from "react";

const LazyComponent = lazy(() => import("@/components/LazyComponent"));

//lazy comp is not loaded prior, click button and lazycomp becomes visible
export default function Page() {
  const [isVisible, setIsVisible] = useState(false);
  return (
    <div>
      <button onClick={() => setIsVisible(true)} className="p-2 bg-blue-300 rounded-md">Load Lazy Component</button>
      {isVisible && <LazyComponent />}
    </div>
  );
};

// separate file
export default function LazyComponent()
  return <div>Lazy Component</div>
}
// can wrap lazy comp in suspense if it does take a long time to download
```
